/*
 * DOC1 ActiveX editor.
 */
var ocm = new Object(); // namespace

/*
 * EngageOne 3.0 ActiveX editor JavaScript Object interface
 * 
 * This holds all of the information that needs to be passed to the editor
 */
ocm.Editor1 = function (baseURL, compress, propertiesId, answerId, schemaId, xformId, pubxId, hipId, spellerId, reviewMode, spellMode, spellCheck ) {
		
	// the base servlet URL used by ActiveX editor for HTTP GET and HTTP PUT requests
	this.url = baseURL;
	
	// compression setting for HTTP GET and HTTP PUT
	// 0 for non-compressed, 1 for compressed
	// when not compressed, the editor will send an individual request for each file it needs,
	// and then an individual request for each file it sends back.
	// when compressed, the editor expects to receive all files defined here by SRID in a ZIP file
	// and it will send back files compressed in a ZIP as well.
	this.compress = compress;
	
	// file ids (EngageOne SRID)
	this.properties = propertiesId;
	this.answer = answerId;
	this.schema = schemaId;
	this.xform = xformId;
	this.pubx = pubxId;
	this.hip = hipId;
	
	// the speller xml id (SRID)
	this.spellerId = spellerId;
	
	// review and spell check mode
	this.reviewMode = reviewMode;
	
	// TODO: we do the spell check manually, so this may need to be set to false here.
	this.spellMode = spellMode;
	this.spellCheck = spellCheck;
};

/*
 * Defines the functions available on the ocm.Editor1 JavaScript object
 */
ocm.Editor1.prototype = {
		
		/*
		 * EngageOne 3.0 way to load editor files. This involves creating and sending the 'load' XML message
		 * to the editor
		 */
		sendLoadMessage : function(editor) {
			
			// set up the load message XML
			// TODO remove unnecessary concatenation once we finalize content via XSD
			var xmlMessage = '<?xml version="1.0" encoding="utf-8" ?>'	
								+ '<MessageInfo version="1" name="Load">'
								+    '<ParameterGroup>'
								+		'<Parameter name="BaseURL" value="'+this.url+'" />'
								+		'<Parameter name="PropertiesID" value="'+this.properties+'" />'
								+		'<Parameter name="AnswerID" value="'+this.answer+'" />'
								+		'<Parameter name="SchemaID" value="'+this.schema+'" />'
								+		'<Parameter name="XFormID" value="'+this.xform+'" />'
								+		'<Parameter name="PubxID" value="'+this.pubx+'" />'
								+		'<Parameter name="ReviewMode" value="'+this.reviewMode+'" />'
								+		'<Parameter name="SpellMode" value="'+this.spellMode+'" />'
								+		'<Parameter name="SpellerID" value="'+this.spellerId+'" />'
								+		'<Parameter name="Compress" value="'+this.compress+'" />'
								+		'<Parameter name="SpellDisableClose" value="'+this.spellCheck+'" />'
								+	'</ParameterGroup>'
								+ '</MessageInfo>';
			
			// the result will be a long.
			// TODO: editor integration when editor changes are in place, comment the alert
			//alert(xmlMessage);
			var result = editor.ExecuteMessage(xmlMessage);
			//alert(result);
		
		},
			
		/*
		 * EngageOne 3.0 way to tell the editor to save files. This creates and sends the 'save' XML message
		 * to the editor
		 */
		sendSaveMessage : function (editor) {
			// set up the load message XML
			// TODO remove unnecessary concatenation once we finalize content via XSD
			var xmlMessage = '<?xml version="1.0" encoding="utf-8" ?>'	
								+ '<MessageInfo version="1" name="Save">'
								+    '<ParameterGroup>'
								+		'<Parameter name="BaseURL" value="'+this.url+'" />'
								+		'<Parameter name="AnswerID" value="'+this.answer+'" />'
								+		'<Parameter name="PubxID" value="'+this.pubx+'" />'
								+		'<Parameter name="HipID" value="'+this.hip+'" />'
								+		'<Parameter name="Compress" value="'+this.compress+'" />'
								+	'</ParameterGroup>'
								+ '</MessageInfo>';
			
			// the result will be a long. 
			// TODO: editor integration when editor changes are in place, comment the alert
			// var result = editor.ExecuteMessage(xmlMessage);
			//alert(xmlMessage);
			 var result = editor.ExecuteMessage(xmlMessage);
			 return result;
		},
		
		/*
		 * Validates the document for completion.
		 */
		validate : function(editor) {
			return editor.IsComplete();
		},
		
		/*
		 * Run Spell Check
		 */
		runSpellCheck : function(editor) {
			editor.SpellCheck();
		}
		
};



// 3.0 Editor Object
var newEditor = null;

/*
 * Prepares all of the necessary data for the ActiveX editor in a JavaScript object
 */
function prepareEditorData(baseURL, compress, propertiesId, answerId, schemaId, xformId, pubxId, hipId, spellerId, reviewMode, spellMode, spellCheck)
{
	debug();
	if (!newEditor)
	{
		newEditor = new ocm.Editor1(baseURL, compress, propertiesId, answerId, schemaId, xformId, pubxId, hipId, spellerId, reviewMode, spellMode, spellCheck);
	}
}

/*
 * Sends the ActiveX editor the message to "Load". The "prepareEditorData" function 
 * should have already been called at this point.
 */
function sendLoadMessageToAXEditor()
{
	debug();
	newEditor.sendLoadMessage(document.getElementById("ipe"));
	debug();
}

/*
 * Sends the ActiveX editor the message to "Save". The "prepareEditorData()" and 
 * "sendLoadMessageToAXEditor()" functions should have already been called at this point,
 * in that order.
 */
function sendSaveMessageToAXEditor()
{
	debug();
	return newEditor.sendSaveMessage(document.getElementById("ipe"));
}

/*
 * Sends the ActiveX editor the command to validate
 */
function isValid(displayMessage) {
	debug();
	var valid = newEditor.validate(document.getElementById("ipe")) == 1 ? true : false;
	if (!valid && displayMessage) {
		alertMessage(documentIncompleteMessage);
	}
	return valid;
}

/*
 * Sends the ActiveX editor the command to perform spell check
 */
function runSpellCheck() {
	debug();
	newEditor.runSpellCheck(document.getElementById("ipe"));
}

/*
 * TODO: remove this function for 3.0
 */
function loadContent(reviewModeFlag) {
	debug();
	editor.loadContent(document.getElementById("ipe"), reviewModeFlag);
}

function saveContent() {
	debug();
	sendSaveMessageToAXEditor();
}

function valid(displayMessage) {
	debug();
	var valid = newEditor.validate(document.getElementById("ipe")) == 1 ? true : false;
	if (!valid && displayMessage) {
		alertMessage(documentIncompleteMessage);
	}
	return valid;
}

function onEditWorkItemSubmitAsTemplate(selectedWorkItem) {
	debug();
	if (selectedWorkItem) {
		var dialog = new dojo.widget.byId("deliveryDetails");
		var dialogHeader = new dojo.widget.byId("deliveryTitle");
		var dialogHeaderLabel = deliveryTitleLabel;
		var widget = new ocm.DialogWidget(dialog, dialogHeader,
				dialogHeaderLabel, deliveryOption);
		var controller = new ocm.FormController(widget, function() {
			// generate preview
				// set inputs and variables
				setDeliveryOptionId(deliveryOption.deliveryOptionId);
				// toggle spellCheck flag
				spellCheckWhenSubmit = false;
				// push submit button again
				var btnSubmit = dojo.byId("btnSubmit");
				btnSubmit.click();
			});
		var retVal = controller.load(selectedWorkItem.templateId,
				selectedWorkItem.workItemId, selectedWorkItem.deliveryOptionId);

		// clean-up
		delete dialog;
		delete dialogHeader;
		delete dialogHeaderLabel;
		delete widget;
		delete controller;

		return retVal;
	}
}

function debug(){
	//alert("Hello World!!! DEBUG");
}
