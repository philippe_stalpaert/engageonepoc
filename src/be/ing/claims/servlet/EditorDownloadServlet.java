package be.ing.claims.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

/**
 * Servlet to be used by the ActiveX editor to obtain template files. This replaces functionality provided by
 * the "com.pb.ocm.client.web.action.DocumentAction" struts action.<br/>
 * <br/>
 * <b>PROPERTIES : .../document/115/downloadPropertyXml.action?resourceId=115<br/>
 * PUBX : .../document/117/download_pub.action?resourceId=117<br/>
 * HIP : .../document/116/download_hip.action?resourceId=116<br/>
 * ANSWER : .../document/119/115/downloadTemplateInstance.action<br/>
 * SCHEMA : .../document/120/download_schema.action?resourceId=120<br/>
 * XFORM : .../document/118/download_view.action?resourceId=118<br/>
 * </b><br/>
 * <br/>
 * The HttpRequest object can have the following parameters on HTTP GET:<br/>
 * <ol>
 * <li>compress - This tells this Servlet whether it should compress the response. Values should be "true" or
 * "false". This only applies if there is only one resource id provided; the response is always compressed if
 * there is more than one resource id parameter passed. If not provided, the default is false.</li>
 * <li>properties - the resource id for the properties file</li>
 * <li>pubx - the resource id for the pubx file</li>
 * <li>hip - the resource id for the hip file</li>
 * <li>answer - the resource id for the answer file</li>
 * <li>schema - the resource id for the schema file</li>
 * <li>xform - the resource id for the xform file</li>
 * <li>srid - a generic way to get a resource if you only know the SRID (resource id)</li>
 * </ol>
 * <br/>
 * <br/>
 */
public class EditorDownloadServlet extends HttpServlet
{
    private static final long serialVersionUID = 7504183218228067774L;
    public static final String IDS_FROM_SESSION = "ids.from.session";

    
    /*
     * (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    @Override
    public void init() throws ServletException
    {
        super.init();
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException,
        IOException
    {
        this.handleGet( req, resp );
    }
    
    
   
    
 
    /* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 this.handlePut( req, resp );
	}

	
	private void handleGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException,
        IOException{
    	
		resp.setHeader("pragma", "no-cache");
    	resp.setHeader("Cache-Control","no-cache");
    	resp.setDateHeader("Expires",-1);
    	resp.setContentType("application/zip");
    	resp.setHeader("Content-Disposition", "inline; filename=output.zip");
    	
    	OutputStream output = resp.getOutputStream();
    	ZipOutputStream zOutput = new ZipOutputStream(output);
    	File dir = new File("c:\\tmp\\output\\");
    	for (File child : dir.listFiles()) {
    		String fileName = FilenameUtils.removeExtension(child.getName());
    		zOutput.putNextEntry(new ZipEntry(fileName));
    		InputStream inp = FileUtils.openInputStream(child);
    		IOUtils.copyLarge(inp , zOutput);
    		zOutput.closeEntry();
    		inp.close();
    	}
    	zOutput.finish();
    	zOutput.flush();
    	output.flush();
    	
    }
    
    
	private void handlePut( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException{
		BufferedInputStream input = new BufferedInputStream(req.getInputStream());
		File file = new File("c:\\tmp\\output\\input.zip");
		OutputStream outp = FileUtils.openOutputStream(file);
		IOUtils.copyLarge(input, outp);
		outp.flush();
		outp.close();
		input.close();
		//throw new IOException();
	}
}